from typing import List, Pattern, Optional

from models.episode import Episode
from models.podcast import Podcast


class ParserI:
    def __init__(self, podcast: Podcast):
        self.podcast = podcast

    @staticmethod
    def name() -> str: raise NotImplementedError()

    def fetch_since(self, limit: str = None) -> List[Episode]: raise NotImplementedError()

    @classmethod
    def validate(cls, url_to_validate) -> bool: raise NotImplementedError()

    @staticmethod
    def url_format() -> str: raise NotImplementedError()

    @classmethod
    def generate_url_from_id(cls, id: str) -> Optional[str]: raise NotImplementedError()

    @staticmethod
    def can_generate_url_from_id() -> bool: return False

    @classmethod
    def _compare(cls, found, expected, object_type):
        assert found == expected, "Invalid {}: expected \"{}\" got \"{}\".".format(object_type, expected, found)

    @classmethod
    def _compare_re(cls, found, regexp: Pattern, object_type):
        assert regexp.match(found), ("Invalid {}: expected to match regex \"{}\" got \"{}\"."
                                     .format(object_type, regexp.pattern, found))
