import itertools
import re
from typing import override
from urllib.parse import urlparse, urljoin

import lxml.html
import requests
from dateutil.parser import parserinfo, parse

from parsers.parser_interface import ParserI, List, Episode, Podcast


class IlPost(ParserI):
    # Example: https://www.ilpost.it/podcasts/morning/
    _SCHEME = "https"
    _HOSTNAME = "www.ilpost.it"
    _PATH_RE = re.compile(r"/podcasts/(\w+)/")
    _EPISODE_DIV_XPATH = r'//div[@class="ilpostPodcastList"]/div'
    _EPISODE_GET_ID_XPATH = r'.//a[@class="play"]/@data-id'
    _EPISODE_GET_TITLE_XPATH = r'.//a[@class="play"]/@data-title'
    _EPISODE_GET_URL_XPATH = r'.//a[@href]/@href'
    _EPISODE_GET_DATE_DURATION_XPATH = r'.//p/text()'

    @override
    def __init__(self, podcast: Podcast):
        super().__init__(podcast)
        self.validate(self.podcast.url)
        self.session = requests.session()

    @staticmethod
    @override
    def name() -> str: return "ilpost"

    @override
    def fetch_since(self, limit: str = None) -> List[Episode]:
        lst = []
        finished = False
        for page in itertools.count(1):
            episodes = self.parse_page(page)
            if limit is not None and episodes[-1].id <= limit:
                episodes = [ep for ep in episodes if ep.id > limit]
                finished = True
            lst.extend(episodes)
            if finished:
                break
        return lst

    def parse_page(self, page_id: int) -> List[Episode]:
        url = self.make_page_url(page_id)
        page = self.session.get(url)
        html = lxml.html.fromstring(page.content)
        return list(map(lambda node: self.parse_episode(node), html.xpath(self._EPISODE_DIV_XPATH)))

    @classmethod
    def parse_date_ita(cls, date):
        return parse(date, parserinfo=parserinfo_ita())

    def parse_episode(self, node) -> Episode:
        id = node.xpath(self._EPISODE_GET_ID_XPATH)[0]
        title = node.xpath(self._EPISODE_GET_TITLE_XPATH)[0]
        url = node.xpath(self._EPISODE_GET_URL_XPATH)[0]
        date_and_duration = node.xpath(self._EPISODE_GET_DATE_DURATION_XPATH)[0]
        date, duration = date_and_duration.split(" - ")
        duration = int(duration.split(" ")[0])
        date = self.parse_date_ita(date).date().isoformat()
        return Episode(id, title, url, date, duration, podcast=self.podcast)

    def make_page_url(self, page_id: int = 1):
        return urljoin(self.podcast.url, "./page/{}/".format(page_id))

    @classmethod
    def _validate(cls, url_to_validate):
        url = urlparse(url_to_validate)
        cls._compare(url.hostname, cls._HOSTNAME, "hostname")
        cls._compare(url.scheme, cls._SCHEME, "scheme")
        cls._compare_re(url.path, cls._PATH_RE, "path")

    @classmethod
    @override
    def validate(cls, url_to_validate) -> bool:
        try:
            cls._validate(url_to_validate)
            return True
        except AssertionError:
            return False

    @staticmethod
    @override
    def url_format(): return "https://www.ilpost.it/podcasts/<TITLE>/"

    @classmethod
    @override
    def generate_url_from_id(cls, id: str) -> str:
        return cls.url_format().replace("<TITLE>", id)

    @staticmethod
    @override
    def can_generate_url_from_id() -> bool: return True


# noinspection PyPep8Naming
class parserinfo_ita(parserinfo):
    MONTHS = ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic']

    def __init__(self):
        super().__init__(dayfirst=True, yearfirst=False)
