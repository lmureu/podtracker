from .ilpost import IlPost
from .parser_interface import ParserI

parsers = {
    "ilpost": IlPost
}


def get_parser_instance(parser_id, podcast) -> ParserI:
    return parsers[parser_id](podcast)
