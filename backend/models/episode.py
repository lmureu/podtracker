from dataclasses import asdict, dataclass

from . import Podcast


@dataclass
class Episode:
    """
    Represent a single episode.

    id: unique episode identifier.
    title: the episode title
    url: the url to listen to the episode
    date: upload date (ISO 8601 format)
    duration: episode duration in minutes
    watched: True if the episode is marked as watched
    podcast: the podcast of which this episode is part.
    """

    id: str
    title: str
    url: str
    date: str
    duration: int
    podcast: Podcast
    watched: bool = False

    def short_form(self):
        from textwrap import shorten
        watched = "[x]" if self.watched else "[ ]"
        return shorten(f"{watched} {self.id} - {self.title}", width=70, placeholder='...')

    def as_dict(self):
        result = asdict(self)
        result["podcast_id"] = result["podcast"]["id"]
        del result["podcast"]
        return result
