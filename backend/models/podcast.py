from dataclasses import dataclass, asdict


@dataclass
class Podcast:
    """
    Represent a podcast.

    name: the name of the podcast
    url: the url of the podcast
    parser: the parser to be used
    """
    id: str
    name: str
    url: str
    parser: str

    def as_dict(self):
        return asdict(self)
