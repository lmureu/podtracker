import sqlite3
from typing import List, Optional, Dict

from parsers import ParserI
from . import Episode, Podcast
from itertools import chain


class Database:
    def __init__(self, db_file=":memory:"):
        self.connection = sqlite3.connect(db_file)
        with self.connection as conn:
            conn.executescript(self.STARTUP_SCRIPT)

    def parsers(self) -> List[str]:
        cursor = self.connection.cursor()
        cursor.execute(self.QUERY_PARSERS)
        return list(chain.from_iterable(cursor.fetchall()))

    def podcasts(self) -> List[Podcast]:
        cursor = self.connection.cursor()
        cursor.execute(self.QUERY_PODCASTS)
        return list(map(lambda t: Podcast(*t), cursor.fetchall()))

    @staticmethod
    def make_episode(episode_partial_tuple: tuple, podcast: Podcast):
        keys = ["id", "title", "url", "date", "duration", "watched", "podcast"]
        values = episode_partial_tuple + (podcast,)
        return Episode(**dict(zip(keys, values)))

    def episodes(self, podcast: Podcast) -> List[Episode]:
        cursor = self.connection.cursor()
        cursor.execute(self.QUERY_EPISODES, podcast.as_dict())
        return list(map(lambda t: self.make_episode(t, podcast=podcast), cursor.fetchall()))

    def last_episode(self, podcast: Podcast) -> Optional[Episode]:
        cursor = self.connection.cursor()
        cursor.execute(self.QUERY_LAST_EPISODE, podcast.as_dict())
        return self.make_episode(cursor.fetchone(), podcast=podcast)

    def add_episode(self, episode: Episode):
        return self.add_episodes([episode])

    def add_parsers(self, parsers: Dict[str, ParserI]):
        values = list(map(lambda k: (k,), parsers.keys()))
        with self.connection as conn:
            conn.executemany(self.INSERT_PARSERS, values)

    def add_episodes(self, episodes: List[Episode]):
        with self.connection as conn:
            conn.executemany(self.INSERT_EPISODE, tuple(map(lambda ep: ep.as_dict(), episodes)))

    def add_podcast(self, podcast: Podcast):
        with self.connection as conn:
            conn.execute(self.INSERT_PODCAST, podcast.as_dict())

    STARTUP_SCRIPT = """
    pragma foreign_keys = on;
    create table if not exists parsers (
        parser text primary key
    );
    create table if not exists podcasts (
        id text primary key,
        name text not null,
        url text not null,
        parser text not null,
        foreign key (parser) references parsers (parser)
    );
    create table if not exists episodes (
        id text,
        title text not null,
        url text not null,
        date text not null,
        duration integer,
        watched boolean default false,
        podcast_id text not null,
        primary key (id, podcast_id),
        foreign key (podcast_id) references podcasts (id)
    );
    """
    QUERY_PARSERS = "SELECT parser FROM PARSERS"
    QUERY_PODCASTS = "SELECT * FROM PODCASTS"
    QUERY_EPISODES = "SELECT id, title, url, date, duration, watched FROM EPISODES where podcast_id = :id"
    QUERY_LAST_EPISODE = ("SELECT * FROM EPISODES WHERE (id, podcast_id) = "
                          "(SELECT MAX(id),podcast_id FROM EPISODES where podcast_id = :id)")
    INSERT_EPISODE = """INSERT INTO EPISODES (id, title, url, date, duration, watched, podcast_id)
                        VALUES (:id, :title, :url, :date, :duration, :watched, :podcast_id)"""
    INSERT_PODCAST = "INSERT INTO PODCASTS (id, name, url, parser) VALUES (:id, :name, :url, :parser)"
    INSERT_PARSERS = "INSERT OR IGNORE INTO PARSERS (PARSER) VALUES (?)"
